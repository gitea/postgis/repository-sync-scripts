
.PHONY: help
help: ## Show this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

sync: ## Mirror SVN to all git remotes
	./git-sync.sh

start-daemon: ## Start the synchronization daemon
	nohup ./update-daemon :postgis > git-sync.log 2>&1 &

start-daemon-if-needed: ## Start the synchronization daemon if needed
	@ps xa | grep -v grep | grep -q 'update-daemon :postgis' && echo "Deamon already running" || make start-daemon
