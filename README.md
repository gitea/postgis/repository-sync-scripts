# Scripts to sync code between repositories

Tested successfully with:

  - git-svn version 2.1.4
  - git-svn version 2.11.0 (svn 1.9.5)

## Initialize the repository

Run `make init` (just `make` for help)

Doing so will clone the repository for it's osgeo official GIT
mirror and initialize the git-svn machinery.

If you do already have a postgis repository clone you might speed
up this step by defining a `POSTGIS_GIT_REF` env variable, like this:

    POSTGIS_GIT_REF=$EXISTING_REPO/.git/objects make init

## Synchronize

Synchronizing manually (which may help setting up and storing
user/password for https access and verifying ssh access)
is done by runnning `make sync`

## Web hook daemon

Make sure synchronization can happen in non-interactive mode.
This usually means adding public key of the user running the
syncronization daemon and configuring the username/password
pair for pushing via https to Gogs. Running the syncrhonization
manually once should help you getting there.

When non-interactive sync works, you can run the daemon with:

    make start-daemon

The script will create an `update` pipe file, world writeable,
and will read from it, synchronizing after 10 seconds of
inactivity from last event read.

You'll probably want to setup a CGI script or similar to write
an event line into that pipe whenever SVN repository is updated.
An example such CGI script is `update-mirrors.cgi` (use with care!)
