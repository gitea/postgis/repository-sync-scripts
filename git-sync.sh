#!/bin/bash

mirrors="gitea_com"

cd /usr/src/postgis
{

 date
 cd postgis.git
 git fetch -p origin

 echo "MIRRORING"
 for remote in $mirrors; do
  cmd="git push --mirror $remote"
  echo $cmd
  $cmd
 done

}
