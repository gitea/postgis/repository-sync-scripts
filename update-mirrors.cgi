#!/bin/sh

echo "Content-Type: text/html"
echo

exec 2>&1
rev=`echo ${PATH_INFO} | cut -c2-`
msg="request from ${REMOTE_ADDR} (${rev})"
echo "$msg" >> /usr/src/postgis/updates &
echo "Mirroring script has been notified about the update"
